import os
from app.web_risk_api_lookup import WebRiskLookup
import json
import pdb

#read env variable, or simply pass the key
GCP_API_KEY = os.getenv('GCP_API_KEY')

#Create WebRiskLookup instance and pass the GCP_API_KEY
wr_inst = WebRiskLookup(api_key=GCP_API_KEY)
"""
curl -H "Content-Type: application/json" "https://webrisk.googleapis.com/v1beta1/uris:search?key=YOUR_API_KEY&threatTypes=MALWARE&uri=http%3A%2F%2Ftestsafebrowsing.appspot.com%2Fs%2Fmalware.html"
"""

#Call lookup url and pass the url you want to check
req1 = wr_inst.lookup(url_to_check='http://testsafebrowsing.appspot.com/s/phishing.html')
req2 = wr_inst.lookup(url_to_check='https://testsafebrowsing.appspot.com/s/trick_to_bill.html')
print(req1)
print(req2)

#if a threat is detected a dictionary is returned. If no threats are detected the dictionary is empty {}
