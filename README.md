# SmallGoogleWebRiskApiWrapper
Lightweight python wrapper to call Google Web Risk Api via urllib. This allows for usage from within GCP (common python package request usually doesn&#39;t work due to GCPs internal routing, at least in the context of GAE)

*How to use the wrapper*

1. Import WebRiskLookup from web_risk_api_lookup
2. Create an instance of WebRiskLookup and pass the api_key.
    To use the Web Risk Api you need to first create an API_KEY within the Google Console
      Navigate to the APIs & Services→Credentials panel in Cloud Console.
      Select Create credentials, then select API key from the dropdown menu.
      The API key created dialog box displays your newly created key.
      You might want to copy your key and keep it secure. Unless you are using a testing key that you intend to delete later, add application and API key restrictions.
3. call the lookup method and pass the url_to_check
4. If a full dictionary is returned then a threat has been detected. If the dictionary is empty there is no threat.

To see an example call have a look at test_web_risk_api.py

To test you can choose some urls from: http://testsafebrowsing.appspot.com/
