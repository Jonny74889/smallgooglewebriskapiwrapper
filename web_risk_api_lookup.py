import urllib
import urllib.request #replaces urllib2 with python3
import base64
import json
import pdb;

class WebRiskLookup(object):
    """docstring forWebRiskLookup."""
    header = {}
    base_uri = 'https://webrisk.googleapis.com'
    lookup_uri = ''
    api_key = ''
    #https://cloud.google.com/web-risk/docs/reference/rest/v1/ThreatType
    threat_types_all = ['MALWARE', 'SOCIAL_ENGINEERING', 'UNWANTED_SOFTWARE']
    # MALWARE	Malware targeting any platform.
    # SOCIAL_ENGINEERING	Social engineering targeting any platform.
    # UNWANTED_SOFTWARE	Unwanted software targeting any platform.

    def __init__(self, api_key, lookup_uri='/v1/uris:search?'):
        if api_key==None or api_key=='':
            raise Exception('Parameter api_key not set!')
        self.api_key = api_key
        if lookup_uri=='':
            raise Exception('Parameter lookup_uri not set!')
        self.lookup_uri = lookup_uri

    def lookup(self, url_to_check, threat_types=[]):
        """
        curl -H "Content-Type: application/json" "https://webrisk.googleapis.com/v1beta1/uris:search?key=YOUR_API_KEY&threatTypes=MALWARE&uri=http%3A%2F%2Ftestsafebrowsing.appspot.com%2Fs%2Fmalware.html"
        """
        # Page to use for testing: http://testsafebrowsing.appspot.com/

        #build url, with key & threattypes & url_to_check
        url = self.base_uri+self.lookup_uri
        if url_to_check==None or url_to_check=='':
            raise Exception('Parameter url_to_check not set!')
        url = url+'&key='+self.api_key
        if threat_types==[]:
            #set to all
            threat_types = self.threat_types_all
        for entry in threat_types:
            url = url+'&threatTypes='+entry
        url = url + '&uri='+url_to_check

        req = urllib.request.Request(url)
        response = urllib.request.urlopen(req)
        page = response.read()
        #https://cloud.google.com/web-risk/docs/status-codes
        return json.loads(page)
